# Content

This repository provides an example of automated Robot Framework project source code designed for conducting interface tests on a sample SUT, Prestashop.

# Architecture

- `resources` folder: contains all keywords required by the tests: page objects, a toolbox containing methods common to multiple tests, helpers…

# Versions

The tested SUT is:

- Prestashop demo version 1.7.8.7

Tests are using the following libraries:

- Robot Framework 6.0.2
- Browser Library 16.0.2
- squash-tf-services 2.0.0

# Usage

These tests are designed to be run from Squash TM, they are using CUFs (custom fields) and datasets.  
It is possible to execute them locally by using [SquashDataSimulator](https://gitlab.com/henixdevelopment/open-source/extra/squashdatasimulator).

There are two ways to use the tests:

### Manually 

1. Configure the back-office SUT to create a user with a commercial profile.  
This user will be useful during teardown, to cancel orders and to maintain a constant stock.
The home page of this user should be the "Orders" page.

2. Create a project in Squash TM, and write the necessary tests manually. 

    This project uses some CUFs with `test-case` scope, and they are the same for all tests:
    - `presta_url`: the URL of the tested SUT
    - `presta_bo_home_page`: the path of the back-office home page (default value: administration/)
    - `presta_bo_log`: the login of the personalized back-office user
    - `presta_bo_pwd`: the password of the personalized back-office user

    For tests using datasets (retrieved through the `Get Test Param` keyword from the `squash_tf.TFParamService` library) at least one dataset must be created, containing values of your choice which are consistent with the Prestashop management rules (e.g. a valid password must be strictly longer than four characters).
    
    Some transmitted data have a specific format, used by the tests, e.g.:

    - `${checkbox}` should be either `yes` or `y` for the checkbox to be checked,
    - `${gender}` should be `M` for male or `F` for female,
    - dates should have the `dd/mm/YYYY` format.

3. Clone the project into a new repository.

4. Configure the test cases in Squash TM to reference the tests in that new repository.  
Launch an automated execution through the Campaign section.


### Via the Injector, if you have access to this one.

1. If you want to use your own SUT instance, configure the back-office SUT to create a user with a commercial profile.  
This user will be useful during teardown, to cancel orders and maintain a constant stock.
The home page of this user should be the "Orders" page..

2. If you have access, you can find the injector project here [link](https://gitlab.com/henixdevelopment/squash/squash-autom-devops-data-injection). Follow the `squash_cloud_injection/squash_cloud_injection.md` tutorial. You'll need a Squash TM instance with all the recommended plugins, and an empty repository with credentials.  If you use another SUT instance, in Squash TM you will need to replace all CUFs `default_value` with what you have configured in your SUT.
