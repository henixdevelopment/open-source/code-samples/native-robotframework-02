*** Settings ***    # robocop: disable=missing-doc-suite
Resource         ../../resources/setup_teardown.resource
Resource         ../../resources/account_management.resource
Resource         ../../resources/cart_management.resource

Test Setup       Run Keywords    Basic Setup    Complete Log Out
Test Teardown    Delete Customer


*** Variables ***
&{PRODUCT_MUG}    Product=Mug The best is yet to come
...                            Number=5
&{PRODUCT_ILLUSTRATION}    Product=Illustration vectorielle Renard
...                                 Number=1


*** Test Cases ***
# robocop: disable=missing-doc-test-case
Update The Product Quantity In The Cart
    Create An Example Customer And Log In
    Set Initial Cart State    ${PRODUCT_MUG}    ${PRODUCT_ILLUSTRATION}

    Set To Dictionary    ${PRODUCT_MUG}    Number=3    UnitPrice=14,28    TotalProductPrice=42,84
    Set To Dictionary    ${PRODUCT_ILLUSTRATION}    Number=4    UnitPrice=10,80    TotalProductPrice=43,20

    Update Product Quantities    ${PRODUCT_MUG}    ${PRODUCT_ILLUSTRATION}
    Verify That The Cart Contains    ${PRODUCT_MUG}    ${PRODUCT_ILLUSTRATION}
    Verify That The Total Number Of Products Is "7" And The Total Price Including Tax Is "86,04"

Remove A Product From The Cart
    Create An Example Customer And Log In
    Set Initial Cart State    ${PRODUCT_MUG}    ${PRODUCT_ILLUSTRATION}
    Remove Product    ${PRODUCT_MUG}[Product]
    Verify That The Cart Contains    ${PRODUCT_ILLUSTRATION}
    Verify That The Total Number Of Products Is "1" And The Total Price Including Tax Is "10,80"
