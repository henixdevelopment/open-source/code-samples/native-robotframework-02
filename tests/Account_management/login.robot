*** Settings ***    # robocop: disable=missing-doc-suite
Library          squash_tf.TFParamService
Resource         ../../resources/setup_teardown.resource
Resource         ../../resources/navigation.resource
Resource         ../../resources/account_management.resource

Test Setup       Basic Setup
Test Teardown    Delete Customer


*** Test Cases ***
# robocop: disable=missing-doc-test-case
Log in with correct credentials
    &{customer} =    Create Dictionary    gender=F    first_name=Mary    last_name=Smith
    ...                                   email=marysmith@example.com    password=smitty4life
    ...                                   birth_date=01/01/2000    offers=yes    newsletter=yes
    Create A Customer And Log Out    &{customer}
    Log In With An Email And Password    ${customer}[email]    ${customer}[password]
    Verify That Welcome Message Is Name And First Name    ${customer}[first_name]    ${customer}[last_name]

Cannot Log In With An Incorrect Password
    &{customer} =    Create Dictionary    gender=F    first_name=Alice    last_name=Noel
    ...                                   email=alice@noel.com    password=police
    ...                                   birth_date=01/01/1970    offers=yes    newsletter=yes
    Create A Customer And Log Out    &{customer}
    Log In With An Email And Password    alice@noel.com    poluce
    Page Should Be    Login
    Message Should Be Displayed    Échec d'authentification
