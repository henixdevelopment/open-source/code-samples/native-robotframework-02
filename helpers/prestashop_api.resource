*** Settings ***    # robocop: disable=missing-doc-suite
Library    XML
Library    DateTime
Library    Browser
Library    squash_tf.TFParamService
Resource    data_format.resource


*** Variables ***
${API_KEY_CUF_REFERENCE}    IT_CUF_presta_api_key
&{CONVERSION_IDS}    M=1    F=2    yes=1
&{ITEM_ATTRIBUTES}    customer=email    order=reference
@{CUSTOMER_REQUIRED_KEYS}    first_name    last_name    email    password
...                          gender    offers    newsletter    birth_date
@{PRODUCT_REQUIRED_KEYS}    Product    Number
@{REQUEST_DATA_REQUIRED_KEYS}    item_type    filter_key    filter_value


*** Keywords ***
# robocop: disable=missing-doc-keyword,too-long-keyword,empty-lines-between-keywords,too-many-calls-in-keyword
Setup A New Account
    [Documentation]    Creates a customer using the API.
    ...                Required keys in \&{customer}:
    ...                first_name, last_name, email, password : string, must respect PrestaShop format.
    ...                gender : known values are 'M' and 'F'. For other values, the gender will be set to undefined.
    ...                offers, newsletter: if value is 'yes', it will be set to TRUE, else it will be set to FALSE.
    ...                birth_date: date with format %d/%m/%Y, else it will fail.
    ...                Other keys are ignored.

    [Arguments]    &{customer}
    Dictionnary Contains All Required Keys    ${customer}    ${CUSTOMER_REQUIRED_KEYS}

    ${xml_base_str} =    Catenate    <prestashop xmlns:xlink="http://www.w3.org/1999/xlink"></prestashop>

    ${customer_xml} =    Parse Xml    ${xml_base_str}
    ${final_xml} =    Build New Customer XML    ${customer_xml}    &{customer}
    ${body} =    Element To String    source=${final_xml}

    ${customers_endpoint} =    Catenate    ${URL}/api/customers
    &{response} =    Call PrestaShop API   POST    ${customers_endpoint}    ${body}
    Should Be Equal As Integers    ${response.status}    201

Setup The Session Cart For A Customer
    [Arguments]    @{products}
    [Documentation]    Creates a cart with products using the API for the customer.
    ...                with email \${USER_MAIL}. If the cart was not empty, this keyword
    ...                will delete all products in the cart before adding the new ones.
    ...                \@{products} is a list of dictionnaries \&{product}.
    ...                Required keys in \&{product}:
    ...                Product: string, product name
    ...                Number: integer, product quantity
    ...                Other keys are ignored.

    ${id_cart} =    Get Session Cart Id For Customer

    ${cart_endpoint} =    Catenate    ${URL}/api/carts/${id_cart}
    &{response_get_cart} =     Call Prestashop Api    GET    ${cart_endpoint}    ${NONE}
    Should Be Equal As Integers    ${response_get_cart.status}    200

    ${cart_xml} =    Parse Xml    ${response_get_cart.body}
    ${cart_with_products} =    Add Basic Products To Cart    ${cart_xml}    @{products}

    ${body} =    Element To String    source=${cart_with_products}

    &{response_put_in_cart} =    Call PrestaShop API   PUT    ${cart_endpoint}    ${body}
    Should Be Equal As Integers    ${response_put_in_cart.status}    200

Delete PrestaShop Item
    [Arguments]    ${item_type}    ${attribute}

    &{delete_request_data} =    Create Dictionary    item_type=${item_type}
    ...                 filter_key=${ITEM_ATTRIBUTES}[${item_type}]
    ...                 filter_value=${attribute}

    ${item_id} =    Extract PrestaShop Item Id    &{delete_request_data}

    ${item_type_endpoint} =    Catenate    ${item_type}s
    ${delete_endpoint} =    Catenate    ${URL}/api/${item_type_endpoint}/${item_id}
    &{response} =    Call PrestaShop API    DELETE    ${delete_endpoint}    ${NONE}
    Should Be Equal As Integers   ${response.status}    200

# BUILD XML PAYLOAD HELPERS #

Build New Customer XML
    [Arguments]    ${xml}    &{customer}
    Add Element    source=${xml}    xpath=.
    ...            element=<customers></customers>
    Add Element    source=${xml}    xpath=customers
    ...            element=<id_default_group><![CDATA[3]]></id_default_group>
    Add Element    source=${xml}    xpath=customers
    ...            element=<active><![CDATA[1]]></active>
    Add Element    source=${xml}    xpath=customers
    ...            element=<id_shop><![CDATA[1]]></id_shop>

    ${final_xml} =    Add Customer Data To Customer XML    ${xml}    &{customer}

    RETURN    ${final_xml}

Add Customer Data To Customer XML    # robocop: disable=too-many-calls-in-keyword
    [Arguments]    ${xml}    &{customer}

    Add Element    source=${xml}    xpath=customers
    ...            element=<firstname><![CDATA[${customer}[first_name]]]></firstname>
    Add Element    source=${xml}    xpath=customers
    ...            element=<lastname><![CDATA[${customer}[last_name]]]></lastname>
    Add Element    source=${xml}    xpath=customers
    ...            element=<email><![CDATA[${customer}[email]]]></email>
    Add Element    source=${xml}    xpath=customers
    ...            element=<passwd><![CDATA[${customer}[password]]]></passwd>

    ${gender_id} =    Convert Property To Property Id    ${customer}[gender]
    ${offers_id} =    Convert Property To Property Id    ${customer}[offers]
    ${newsletter_id} =    Convert Property To Property Id    ${customer}[newsletter]
    ${formatted_birth_date} =    Convert Date    ${customer}[birth_date]
    ...                          date_format=%d/%m/%Y    result_format=%Y-%m-%d

    Add Element    source=${xml}    xpath=customers
    ...            element=<birthday><![CDATA[${formatted_birth_date}]]></birthday>
    Add Element    source=${xml}    xpath=customers
    ...            element=<id_gender><![CDATA[${gender_id}]]></id_gender>
    Add Element    source=${xml}    xpath=customers
    ...            element=<optin><![CDATA[${offers_id}]]></optin>
    Add Element    source=${xml}    xpath=customers
    ...            element=<newsletter><![CDATA[${newsletter_id}]]></newsletter>

    RETURN    ${xml}

Add Basic Products To Cart
    [Arguments]    ${cart_xml}    @{products}
    [Documentation]    This keyword adds products to a cart xml structure. WARNING:
    ...                Only simple product (id and quantity) works for now.
    ...                Products with attributes (size, color etc...) will break the code.

    Remove Element    ${cart_xml}    xpath=cart/associations
    Add Element    source=${cart_xml}    xpath=cart
    ...            element=<associations></associations>
    Add Element    source=${cart_xml}    xpath=cart/associations
    ...            element=<cart_rows></cart_rows>

    ${i} =    Convert To Integer    1
    FOR    ${product}    IN    @{products}
        Dictionnary Contains All Required Keys    ${product}    ${PRODUCT_REQUIRED_KEYS}

        &{get_product_request_data} =    Create Dictionary    item_type=product
        ...                 filter_key=name
        ...                 filter_value=${product}[Product]

        ${product_id} =    Extract PrestaShop Item Id    &{get_product_request_data}

        Add Element    source=${cart_xml}    xpath=cart/associations/cart_rows
        ...            element=<cart_row></cart_row>

        Add Element    source=${cart_xml}    xpath=cart/associations/cart_rows/cart_row[${i}]
        ...            element=<id_product><![CDATA[${product_id}]]></id_product>

        Add Element    source=${cart_xml}    xpath=cart/associations/cart_rows/cart_row[${i}]
        ...            element=<quantity><![CDATA[${product}[Number]]]></quantity>
        ${i} =    Evaluate    ${i}+1
    END
    RETURN    ${cart_xml}

# HELPERS FOR CART #

Get Session Cart Id For Customer
    ${guest_id} =    Get Session Guest Id For Customer
    &{get_cart_request_data} =    Create Dictionary    item_type=cart
    ...                 filter_key=id_guest
    ...                 filter_value=${guest_id}

    ${id_cart} =    Extract PrestaShop Item Id    &{get_cart_request_data}

    RETURN    ${id_cart}

Get Session Guest Id For Customer
    &{get_customer_request_data} =    Create Dictionary    item_type=customer
    ...                 filter_key=email
    ...                 filter_value=${CUSTOMER_MAIL}
    ${id_customer} =    Extract PrestaShop Item Id    &{get_customer_request_data}

    &{get_guest_request_data} =    Create Dictionary    item_type=guest
    ...                    filter_key=id_customer
    ...                    filter_value=${id_customer}
    ${id} =    Extract PrestaShop Item Id    &{get_guest_request_data}
    RETURN    ${id}

# SHARED HELPERS KEYWORDS #

Extract PrestaShop Item Id
    [Arguments]    &{request_data}
    [Documentation]    This keyword call the API on the url \${URL}/api/\${request_data}[item_type]s
    ...                with the filter \${request_data}[filter_key]=\${request_data}[filter_value]
    ...                expecting to find one element. If no or several objects are found, the test fails.
    ...                If one and only one element is found, the keyword returns its id.
    ...                Required keys in \&{request_data} :
    ...                item_type: string, singular, the endpoint of item we look for (i.e.: customer, cart...)
    ...                filter_key: the key of attribute we look for (i.e.: email)
    ...                filter_value: the value of the attribute we look for (i.e.: anemail@mail.fr)
    Dictionnary Contains All Required Keys    ${request_data}    ${REQUEST_DATA_REQUIRED_KEYS}
    @{elements} =    Search PrestaShop Item    &{request_data}
    ${number_of_elements} =    Get Length    ${elements}

    ${error_message_too_many_elements} =    Catenate    Several elements found with ${request_data}[filter_key]
    ...                                     = ${request_data}[filter_value] when calling
    ...                                     ${URL}/api/${request_data}[item_type]s.

    ${error_message_no_element} =    Catenate    No element found with ${request_data}[filter_key]
    ...                                          = ${request_data}[filter_value] when calling
    ...                                          ${URL}/api/${request_data}[item_type]s.

    IF    ${number_of_elements} == 0    Fail    ${error_message_no_element}
    IF    ${number_of_elements} > 1    Fail    ${error_message_too_many_elements}

    ${id} =    XML.Get Element Attribute    ${elements}[0]    id
    RETURN    ${id}

Search PrestaShop Item
    [Arguments]    &{request_data}
    Dictionnary Contains All Required Keys    ${request_data}    ${REQUEST_DATA_REQUIRED_KEYS}
    ${item_endpoint} =    Catenate    ${request_data}[item_type]s

    ${endpoint} =    Catenate
    ...        ${URL}/api/${item_endpoint}?filter[${request_data}[filter_key]]=${request_data}[filter_value]

    &{response} =    Call PrestaShop API   GET    ${endpoint}    ${NONE}
    Should Be Equal As Integers    ${response.status}    200
    @{elements} =    XML.Get Elements    ${response.body}    .//${request_data}[item_type]
    RETURN    ${elements}

Call Prestashop Api
    [Arguments]    ${http_method}    ${endpoint}    ${body}
    &{headers} =    Create PrestaShop Api Headers
    &{response} =    Http    method=${http_method}    url=${endpoint}
    ...                      headers=${headers}       body=${body}
    IF    ${response.ok} != ${TRUE}
        ${error_message} =    Catenate    ${http_method} request '${endpoint}' FAILED,
        ...                   status: ${response.status}, ${\n} body: ${\n}
        ...                   ${response.body}
        Fail    ${error_message}
    END
    RETURN    ${response}

Create PrestaShop Api Headers
    [Documentation]    This keyword retrieves the PrestaShop API Key that is defined in Squash as a CUF,
    ...                builds the prestashop API credentials needed to use the PrestaShop API,
    ...                and returns the \&{headers} used in PrestaShop API requests.

    ${api_key} =    Get Test Param    ${API_KEY_CUF_REFERENCE}
    ${api_credentials} =    Catenate    ${api_key}:
    ${api_credentials_b64} =    Evaluate    base64.b64encode(bytes('${api_credentials}', 'utf-8'))
    &{headers} =    Create Dictionary    Content-type=text/xml    Authorization=Basic ${api_credentials_b64}
    RETURN    ${headers}

Convert Property To Property Id    # robocop: disable=too-few-calls-in-keyword
    [Arguments]   ${property}
    IF    "${property}" in "${CONVERSION_IDS}"    RETURN    ${CONVERSION_IDS}[${property}]
    RETURN    0
